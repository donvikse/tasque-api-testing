import { ApiRequest } from "../request";
let baseUrl: string= 'http://tasque.lol/';
export class CoursesController {
    
    //Зареєструвати юзера
   async RegisterController(emailValue: string, passwordValue: string, name: string) {
            const response = await new ApiRequest()
                .prefixUrl(baseUrl)
                .method("POST")
                .url(`api/Register`)
                .body({
                    email: emailValue,
                    password: passwordValue,
                    userName: name
                })
                .send();
            return response;
        }
     // Отримати всіх юзерів   
       async getAllUsers(){
            const response = await new ApiRequest()
                .prefixUrl(baseUrl)
                .method("GET")
                .url(`api/Users`)
                .bearerToken()
                .send();
            return response;
        }
      //Отримати юзера по id  
        async getUserbyid(id:string){
            const response = await new ApiRequest()
                .prefixUrl(baseUrl)
                .method("GET")
                .url(`api/Users/${id}`)
                .bearerToken()
                .send();
            return response;
        }
        // Авторизуватися
        async AuthUser(emailValue: string, passwordValue: string){
            const response = await new ApiRequest()
                .prefixUrl(baseUrl)
                .method("POST")
                .url(`api/Auth/login`)
                .bearerToken()
                .body({
                    email: emailValue,
                    password: passwordValue
                })
                .send();
            return response;
        }  
        //Отримати деталі поточного користувача
        async getUserbytoken(token:string){
            const response = await new ApiRequest()
                .prefixUrl(baseUrl)
                .method("GET")
                .url(`api/Users/fromToken`)
                .bearerToken(token)
                .send();
            return response;
        }
        //Оновити поточного користувача
        async putUser(userId:string, avatarka: string, emailValue:string, name:string, token:string){
            const response = await new ApiRequest()
                .prefixUrl(baseUrl)
                .method("PUT")
                .url(`api/Users`)
                .body({
                    id: userId,
                    avatar: avatarka, 
                    email: emailValue,
                    userName: name
                })
                .bearerToken(token)
                .send();
            return response;
         }
         // Видалити поточного користувача
         async deleteUserbyid(id:string, token:string){
            const response = await new ApiRequest()
                .prefixUrl(baseUrl)
                .method("DELETE")
                .url(`api/Users/${id}`)
                .bearerToken(token)
                .send();
            return response;
        }
        // Отримати всі пости
        async getAllPosts(){
            const response = await new ApiRequest()
                .prefixUrl(baseUrl)
                .method("GET")
                .url(`api/Posts`)
                .bearerToken()
                .send();
            return response;
}
         //Створити новий пост  
         async newPost(author: string, image1: string, body1:string, token:string){
            const response = await new ApiRequest()
                .prefixUrl(baseUrl)
                .method("POST")
                .url(`api/Posts`)
                .bearerToken(token)
                .body({
                    authorId: author,
                    previewImage: image1,
                    body: body1
                })
                .send();
            return response;
        }  
        //Поставити лайк посту
        async likePost(entity: string, like: boolean, id1:string, token:string){
            const response = await new ApiRequest()
                .prefixUrl(baseUrl)
                .method("POST")
                .url(`api/Posts/like`)
                .bearerToken(token)
                .body({
                    entityId: entity,
                    islike: like,
                    userId: id1
                })
                .send();
            return response;
        }  
        //Додати коментар
        async commentPost(author: string, post: string, body1:string, token:string){
            const response = await new ApiRequest()
                .prefixUrl(baseUrl)
                .method("POST")
                .url(`api/Comments`)
                .bearerToken(token)
                .body({
                    authorId: author,
                    postId: post,
                    body: body1
                })
                .send();
            return response;
        }  
}

