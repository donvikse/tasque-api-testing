import { expect } from "chai";
import {checkResponseTime, checkStatusCode} from "../../helpers/functionsForChecking.helper";
import { CoursesController } from "../lib/controllers/courses.controller";
const courses=new CoursesController();
//const schemas=require('./data/schemas_testData.json');
//var chai=require('chai');
//chai.use(require('chai-json-schema'));
describe("My tests ",()=>{
    let userId: string;
    let userToken: string;
    let newpostId: string;
     //Зареєструватися
    it('Register', async()=>{
   let response=await courses.RegisterController("don123456@gmail.com", "V1i9k8a4", "Vika");
       console.log(response.body);
       checkStatusCode(response,201);
       checkResponseTime(response,3000);
       expect (response.body.user.userName,"Checking if the username is expected Vika").to.equal("Vika");
    });
    //Авторизуватися
   it('Login', async()=>{
    let response=await courses.AuthUser("don123456@gmail.com", "V1i9k8a4");
    userId=response.body.user.id;
    userToken=response.body.token.accessToken.token;
    checkStatusCode(response,200);
   checkResponseTime(response,3000);
   });
    //Отримати юзера за id
   it('Get user by id', async()=>{
       let response=await courses.getUserbyid(userId);
       checkStatusCode(response,200);
       checkResponseTime(response,3000);
    });
   //Отримати деталі поточного користувача за токеном
   it('Get user by token', async()=>{
    let response=await courses.getUserbytoken(userToken);
       checkStatusCode(response,200);
       checkResponseTime(response,3000);
   });
   //Оновити поточного користувача
   it('Update user', async()=>{
    let response=await courses.putUser(userId," ","donvikse1@gmail.com","Rita", userToken);
    checkStatusCode(response,204);
       checkResponseTime(response,3000);
   });
   //Отримати всі пости
  it('Get all posts', async()=>{
    let response=await courses.getAllPosts();
    newpostId=response.body[0].id;
    //console.log(newpostId);
    checkStatusCode(response,200);
       checkResponseTime(response,3000);
    //expect(response.body).to.be.jsonSchema(schemas.schema_postInfo);
     });
  //Створити новий пост    
   it('New post', async()=>{
    let response=await courses.newPost(userId, "picture", "My post", userToken);
   
    checkStatusCode(response,200);
       checkResponseTime(response,3000);
 });
 // Поставити лайк посту
   it('Like post', async()=>{
    let response=await courses.likePost(newpostId, true, userId, userToken);
    checkStatusCode(response,200);
       checkResponseTime(response,3000);
    expect(response.body.islike, "Check that the reaction is set").not.equal(null);
 });
 // Поставити коментар
 it('Comment post', async()=>{
   let response=await courses.commentPost(userId, newpostId , "It is the best post", userToken)
   checkStatusCode(response,200);
       checkResponseTime(response,3000);
 });
 // Видалити юзера по id
 it('Delete user', async()=>{
    let response=await courses.deleteUserbyid(userId,userToken);
    checkStatusCode(response,204);
       checkResponseTime(response,3000);
 });
})